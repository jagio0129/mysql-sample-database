MySQL Sample DatabaseのデータをDBに反映させる方法
===

[ここ](https://www.mysqltutorial.org/mysql-sample-database.aspx)の「Download MySQL Sample Database」からmysqlsampledatabase.sqlをダウンロードします。

以下のようなテーブルが作成できます

![](https://sp.mysqltutorial.org/wp-content/uploads/2009/12/MySQL-Sample-Database-Schema.png)

ダウンロードした.sqlファイルをdocker/db/sqlにコピーします。あとは立ち上げるだけで.sqlのデータがDBに反映されます。

```
# mysqlへログイン
docker exec -it (docker ps -q) mysql -u root -p -D example -P 3337
```
